﻿namespace QB.Backend.Common.Helpers;

public static class EnumHelper
{
    public static List<int> GetFlagsOfEnum(this Enum @enum)
    {
        var flagsValue = new[] { @enum }.Cast<int>().First();

        return Enum.GetValues(@enum.GetType())
            .Cast<int>()
            .Where(option => option != 0 && (option & flagsValue) == option)
            .ToList();
    }

    public static TEnum GetFlaggedEnumFromList<TEnum>(this List<int> source) where TEnum: Enum
    {
        if (source is null || !source.Any())
        {
            return default;
        }

        int result = 0;

        foreach (var option in source)
        {
            result |= option;
        }

        return new[] { result }.Cast<TEnum>().First();
    }
}