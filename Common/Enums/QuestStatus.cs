﻿namespace QB.Backend.Common.Enums;

public enum QuestStatus
{
    New,
    Complete,
    Deleted
}