﻿using System.ComponentModel;

namespace QB.Backend.Common.Enums;

[Flags]
public enum QuestType
{
    [Description("Фитнес и спорт")]
    Fitness = 1 << 0,
    [Description("Образование")]
    Education = 1 << 1,
    [Description("Готовка")]
    Food = 1 << 2,
    [Description("Правильное питание")]
    Eating = 1 << 4,
    [Description("Квесты")]
    Quest = 1 << 5,
    [Description("Поведение")]
    Behavior = 1 << 6,
}