﻿namespace QB.Backend.Common.Enums;

public enum RewardType
{
    Main,
    Additional
}