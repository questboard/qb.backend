﻿namespace QB.Backend.Common.Enums;

public enum Gender
{
    Female,
    Male
}