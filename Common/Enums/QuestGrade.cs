﻿namespace QB.Backend.Common.Enums;

public enum QuestGrade
{
    Normal = 10,
    Special = 12,
    Epic = 17,
    Legendary = 25
}