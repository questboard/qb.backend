﻿using Microsoft.EntityFrameworkCore;
using QB.Backend.Domain;

namespace QB.Backend.DataAccess;

public class QuestBoardDb : DbContext
{
    public QuestBoardDb(DbContextOptions<QuestBoardDb> options)
        : base(options)
    {}
    
    public DbSet<Character> Characters { get; set; }
    public DbSet<QuestTemplate> QuestTemplates { get; set; }
    public DbSet<ItemTemplate> ItemTemplates { get; set; }
    public DbSet<RewardTemplate> RewardTemplates { get; set; }
    public DbSet<Quest> Quests { get; set; }
    public DbSet<Reward> Rewards { get; set; }
    public DbSet<InventoryItem> Items { get; set; }
    public DbSet<HistoryRow> HistoryRows { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.HasDefaultSchema("public");
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<Quest>()
            .HasMany(a => a.Rewards)
            .WithOne(a => a.Quest)
            .OnDelete(DeleteBehavior.Cascade);

        modelBuilder.Entity<Character>()
            .HasMany(a => a.Quests)
            .WithOne(a => a.Character)
            .OnDelete(DeleteBehavior.Cascade);

        modelBuilder.Entity<Character>()
            .HasMany(a => a.History)
            .WithOne(a => a.Character)
            .OnDelete(DeleteBehavior.Cascade);

        modelBuilder.Entity<Character>()
            .HasMany(a => a.Items)
            .WithOne(a => a.Character)
            .OnDelete(DeleteBehavior.Cascade);
    }
}