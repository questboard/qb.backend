﻿using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using QB.Backend.DataAccess;
using QB.Backend.Domain;

namespace QB.Backend;

internal static class StartupExtensions
{
    public static IApplicationBuilder Migrate(this IApplicationBuilder builder)
    {
        using var scope = builder.ApplicationServices.CreateScope();
        using var context = scope.ServiceProvider.GetRequiredService<QuestBoardDb>();
        context.Database.Migrate();

        return builder;
    }

    public static async Task<IApplicationBuilder> InitData(this IApplicationBuilder builder)
    {
        using var scope = builder.ApplicationServices.CreateScope();
        using var context = scope.ServiceProvider.GetRequiredService<QuestBoardDb>();

        if (!context.ItemTemplates.Any())
            context.ItemTemplates.AddRange(await LoadTemplates<ItemTemplate>("./InitData/itemTemplates.json"));
        
        if (!context.RewardTemplates.Any())
            context.RewardTemplates.AddRange(await LoadTemplates<RewardTemplate>("./InitData/rewardTemplates.json"));
        
        if (!context.QuestTemplates.Any())
            context.QuestTemplates.AddRange(await LoadTemplates<QuestTemplate>("./InitData/questTemplates.json"));
        
        await context.SaveChangesAsync();
        return builder;
    }

    private static async Task<List<T>> LoadTemplates<T>(string fileName)
    {
        if (!File.Exists(fileName))
            return new List<T>();

        await using var stream = File.OpenRead(fileName);

        var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
        options.Converters.Add(new JsonStringEnumConverter());

        return await JsonSerializer.DeserializeAsync<List<T>>(stream, options) ?? new List<T>();
    }
}