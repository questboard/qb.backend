﻿using QB.Backend.Common.Enums;

namespace QB.Backend.Models.Responses;

public class QuestDetailsView
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public DateTime CompleteTill { get; set; }
    public QuestGrade Grade { get; set; }
    public QuestStatus Status { get; set; }
    public List<RewardView> Rewards { get; set; } 
    public List<RewardView> AdditionalRewards { get; set; } 
}