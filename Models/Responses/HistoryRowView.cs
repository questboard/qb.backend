﻿namespace QB.Backend.Models.Responses;

public class HistoryRowView
{
    public string Id { get; set; }
    public string Text { get; set; }
    public DateTime Created { get; set; }
}