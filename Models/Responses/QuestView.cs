﻿using QB.Backend.Common.Enums;

namespace QB.Backend.Models.Responses;

public class QuestView
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string ShortDescription { get; set; }
    public DateTime CompleteTill { get; set; }
    public QuestGrade Grade { get; set; }
    public QuestStatus Status { get; set; }
}