﻿using QB.Backend.Common.Enums;

namespace QB.Backend.Models.Responses;

public class CharacterView
{
    public string Id { get; set; }
    public Gender Gender { get; set; }
    public DateTime BirthDate { get; set; }
    public List<int> QuestTypes { get; set; }
    public DateTime LastQuestRefresh { get; set; }
    public DateTime PremiumTill { get; set; }
    public bool IsPremium { get; set; }
}