﻿namespace QB.Backend.Models.Responses;

public class RewardView
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public int Count { get; set; }
}