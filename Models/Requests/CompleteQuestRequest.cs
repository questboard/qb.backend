﻿namespace QB.Backend.Models.Requests;

public class CompleteQuestRequest
{
    public List<string> AdditionalRewards { get; set; }
}