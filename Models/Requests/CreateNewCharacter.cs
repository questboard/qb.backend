﻿using QB.Backend.Common.Enums;

namespace QB.Backend.Models.Requests;

public class CreateNewCharacter
{
    public string Name { get; set; }
    public Gender Gender { get; set; }
    public DateTime BirthDate { get; set; }
    public List<int> QuestTypes { get; set; }
}