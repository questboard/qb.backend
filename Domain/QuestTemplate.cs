﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using QB.Backend.Common.Enums;

namespace QB.Backend.Domain;

public class QuestTemplate 
{
    [Key]
    public int Id { get; set; }
    public QuestType Type { get; set; }
    public string Name { get; set; }
    public string ShortDescription { get; set; }
    public string Description { get; set; }
    public int Min { get; set; }
    public int Max { get; set; }
    public int DaysToComplete { get; set; }
    
    [JsonIgnore]
    public List<Quest> Quests { get; set; }
}