﻿using QB.Backend.Common.Enums;

namespace QB.Backend.Domain;

public class Character : BaseEntity
{
    public string Name { get; set; }
    public Gender Gender { get; set; }
    public DateTime BirthDate { get; set; }
    public QuestType QuestTypes { get; set; }
    public DateTime LastQuestRefresh { get; set; } = DateTime.UnixEpoch.AddYears(-70);
    public DateTime PremiumTill { get; set; } = DateTime.UnixEpoch.AddYears(-70);
    public bool IsPremium => PremiumTill > DateTime.Now;
    public List<InventoryItem> Items { get; set; } = new();
    public List<Quest> Quests { get; set; } = new();
    public List<HistoryRow> History { get; set; }
}