﻿namespace QB.Backend.Domain;

public class HistoryRow : BaseEntity
{
    public string CharacterId { get; set; }
    public string Text { get; set; }
    public Character Character { get; set; }
}