﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace QB.Backend.Domain;

public class RewardTemplate
{
    [Key]
    public int Id { get; set; }
    public int ItemTemplateId { get; set; }
    public int Min { get; set; }
    public int Max { get; set; }
    [JsonIgnore]
    public ItemTemplate ItemTemplate { get; set; }
    [JsonIgnore]
    public List<Reward> Rewards { get; set; }

    public Reward ToReward()
        => new()
        {
            TemplateId = Id,
            Template = this
        };
}