﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace QB.Backend.Domain;

public class ItemTemplate
{
    [Key]
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    [JsonIgnore]
    public List<InventoryItem> Items { get; set; }
}