﻿namespace QB.Backend.Domain;

public class InventoryItem : BaseEntity
{
    public int Count { get; set; }
    public int ItemTemplateId { get; set; }
    public ItemTemplate Template { get; set; }
    public string CharacterId { get; set; }
    public Character Character { get; set; }
}