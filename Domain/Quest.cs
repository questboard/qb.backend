﻿using QB.Backend.Common.Enums;

namespace QB.Backend.Domain;

public class Quest : BaseEntity
{
    public string CharacterId { get; set; }
    public QuestGrade Grade { get; set; }
    public QuestType Type { get; set; }
    public QuestStatus Status { get; set; }
    public int Count { get; set; }
    public DateTime CompleteTill { get; set; }
    
    public int TemplateId { get; set; }
    public List<Reward> Rewards { get; set; }
    
    public Character Character { get; set; }
    public QuestTemplate Template { get; set; }
}