﻿using QB.Backend.Common.Enums;

namespace QB.Backend.Domain;

public class Reward : BaseEntity
{
    public int Count { get; set; }
    public string QuestId { get; set; }
    public int TemplateId { get; set; }
    
    
    public Quest Quest { get; set; }
    public RewardTemplate Template { get; set; }

    public override string ToString()
    {
        return $"{Template.ItemTemplate.Name} ({Count})";
    }

    public Reward WithCount(QuestGrade grade)
    {
        Count = (int)(new Random().Next(Template.Min, Template.Max) * ((double)grade / 7));
        return this;
    }

    public Reward WithQuestId(string questId)
    {
        QuestId = questId;
        return this;
    }
}