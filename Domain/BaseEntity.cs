﻿using System.ComponentModel.DataAnnotations;

namespace QB.Backend.Domain;

public class BaseEntity
{
    [Key]
    public string Id { get; set; } = Guid.NewGuid().ToString();

    public DateTime Created { get; set; } = DateTime.Now;
}