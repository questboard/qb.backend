﻿using Microsoft.EntityFrameworkCore;
using QB.Backend.DataAccess;

namespace QB.Backend.Services.Hosted;

public class QuestsMonitor : BackgroundService
{
    private readonly IServiceScopeFactory _serviceProvider;
    
    public QuestsMonitor(IServiceScopeFactory serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        try
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                if (DateTime.Now.Hour > 2 && DateTime.Now.Hour < 3)
                {
                    await DoWork(stoppingToken);
                }

                await Task.Delay(TimeSpan.FromMinutes(10), stoppingToken);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    private async Task DoWork(CancellationToken stoppingToken)
    {
        using var scope = _serviceProvider.CreateScope();
        await using var context = scope.ServiceProvider.GetRequiredService<QuestBoardDb>();

        var characters = await context.Characters
            .Include(a => a.Quests)
            .ThenInclude(q => q.Template)
            .ToListAsync(stoppingToken);

        var generator = new QuestGenerator();
        foreach (var character in characters)
        {
            var toRemove = character.Quests
                .Where(q => q.Created < DateTime.Now.AddDays(-q.Template.DaysToComplete))
                .ToList();
            
            if (toRemove.Any())
            {
                context.Quests.RemoveRange(toRemove);
                await context.SaveChangesAsync(stoppingToken);
            }

            for (var i = character.Quests.Except(toRemove).Count(); i < 5; i++)
                await generator.GenerateNew(character, context);
        }
    }
}