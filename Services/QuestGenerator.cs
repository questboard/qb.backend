﻿using Microsoft.EntityFrameworkCore;
using QB.Backend.Common.Enums;
using QB.Backend.Common.Helpers;
using QB.Backend.DataAccess;
using QB.Backend.Domain;
using static QB.Backend.Common.Enums.Gender;

namespace QB.Backend.Services;

public class QuestGenerator
{
    public async Task<Quest> GenerateNew(Character character, QuestBoardDb db)
    {
        var flags = character.QuestTypes.GetFlagsOfEnum();
        var templates = await db.QuestTemplates
            .ToListAsync();

        templates = templates.Where(q => flags.Intersect(q.Type.GetFlagsOfEnum()).Any()).ToList();
        
        var rewards = await db.RewardTemplates
            .Include(a => a.ItemTemplate)
            .ToListAsync();

        var rnd = new Random();
        var template = templates[rnd.Next(0, templates.Count)];
        var questId = Guid.NewGuid().ToString();
        var grade = GetGrade();
        var quest = new Quest
        {
            Id = questId,
            Type = template.Type,
            CharacterId = character.Id,
            TemplateId = template.Id,
            Status = QuestStatus.New,
            Grade = grade,
            Count = GetCountForQuest(template, character, grade),
            CompleteTill = DateTime.Now.AddDays(template.DaysToComplete),
            Rewards = new List<Reward> {rewards[rnd.Next(0, rewards.Count)].ToReward().WithCount(grade).WithQuestId(questId)}
        };

        db.Quests.Add(quest);
        await db.SaveChangesAsync();
        
        return quest;
    }

    private static int GetCountForQuest(QuestTemplate questTemplate, Character character, QuestGrade questGrade)
        => (int)(new Random().Next(questTemplate.Min, questTemplate.Max) * ((double)questGrade / 10) * questTemplate.Type switch
        {
            QuestType.Fitness => ((character.BirthDate - DateTime.Now).Days / 365) switch
            {
                < 10 => 1,
                < 20 => 1.5,
                < 30 => 1.9,
                < 40 => 2.2,
                < 50 => 1.8,
                _ => 1
            } * (character.Gender == Male ? 1.5 : 1),
            _ => 1

        });

    private static QuestGrade GetGrade()
        => new Random().Next(100) switch
        {
            > 97 => QuestGrade.Legendary,
            > 83 => QuestGrade.Epic,
            > 70 => QuestGrade.Special,
            _ => QuestGrade.Normal
        };
}