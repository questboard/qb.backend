using Mapster;
using Microsoft.EntityFrameworkCore;
using QB.Backend;
using QB.Backend.Common.Enums;
using QB.Backend.Common.Helpers;
using QB.Backend.DataAccess;
using QB.Backend.Domain;
using QB.Backend.Models.Requests;
using QB.Backend.Models.Responses;
using QB.Backend.Services;
using QB.Backend.Services.Hosted;

var builder = WebApplication.CreateBuilder(args);

var configuration = builder.Configuration;
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
builder.Services.AddDbContext<QuestBoardDb>(o => 
    o.UseNpgsql(configuration.GetConnectionString("default")));
builder.Services.AddHostedService<QuestsMonitor>();
builder.WebHost.ConfigureKestrel((context, serverOptions) =>
{
    var section = context.Configuration.GetSection("Kestrel");
    serverOptions.Configure(section)
        .Endpoint("HTTPS", opt =>
        {
            
        });
});

TypeAdapterConfig.GlobalSettings.Scan(typeof(Program).Assembly);
var app = builder.Build();
await app.Migrate().InitData();

var characters = app.MapGroup("/characters");
characters.MapPost("/", CreateNew).Produces<CharacterView>();
characters.MapGet("/{id}", GetInfo).Produces<CharacterView>();
characters.MapGet("/{id}/inventory", GetInventory).Produces<List<InventoryItemView>>();
characters.MapGet("/{id}/quests", GetQuests).Produces<List<QuestView>>();
characters.MapPut("/{id}/premium", GetPremium);
characters.MapGet("/{id}/history", GetHistory).Produces<List<HistoryRowView>>();

var quests = app.MapGroup("/quests");
quests.MapGet("/{id}", GetQuest).Produces<QuestDetailsView>();
quests.MapPut("/{id}", CompleteQuest);
quests.MapDelete("/{id}", RefreshQuest);

var admin = app.MapGroup("/admin");
admin.MapDelete("/characters/{id}", DeleteCharacter);
admin.MapDelete("/quests/{id}", DeleteQuest);
admin.MapDelete("/characters/{id}/quests", DeleteQuests);
admin.MapPost("/quests", GenerateQuests);

#region Characters
static async Task<IResult> CreateNew(CreateNewCharacter character, QuestBoardDb db)
{
    var existing = await db.Characters.FirstOrDefaultAsync(a => a.Name == character.Name);

    if (existing is not null)
    {
        await GenerateQuests(new GenerateQuestsRequest{ CharacterId = existing.Id }, db);
        return TypedResults.Ok(new CharacterView
        {
            Id = existing.Id,
            PremiumTill = existing.PremiumTill,
            LastQuestRefresh = existing.LastQuestRefresh,
            IsPremium = existing.IsPremium,
            QuestTypes = existing.QuestTypes.GetFlagsOfEnum(),
            Gender = existing.Gender,
            BirthDate = existing.BirthDate
        });
    }
    
    var ch = db.Characters.Add(new Character
    {
        Name = character.Name,
        Gender = character.Gender,
        BirthDate = character.BirthDate,
        QuestTypes = character.QuestTypes.GetFlaggedEnumFromList<QuestType>()
    });
    await db.SaveChangesAsync();
    
    await GenerateQuests(new GenerateQuestsRequest{ CharacterId = ch.Entity.Id }, db);
    return TypedResults.Ok(new CharacterView
    {
        Id = ch.Entity.Id,
        PremiumTill = ch.Entity.PremiumTill,
        LastQuestRefresh = ch.Entity.LastQuestRefresh,
        IsPremium = ch.Entity.IsPremium,
        QuestTypes = ch.Entity.QuestTypes.GetFlagsOfEnum(),
        Gender = ch.Entity.Gender,
        BirthDate = ch.Entity.BirthDate
    });
}

static async Task<IResult> GetInfo(string id, QuestBoardDb db)
    => TypedResults.Ok(await db.Characters.AsNoTracking().Select(a => new CharacterView
    {
        Id = a.Id,
        PremiumTill = a.PremiumTill,
        LastQuestRefresh = a.LastQuestRefresh,
        IsPremium = a.IsPremium,
        QuestTypes = a.QuestTypes.GetFlagsOfEnum(),
        Gender = a.Gender,
        BirthDate = a.BirthDate
    }).FirstOrDefaultAsync(a => a.Id == id));

static async Task<IResult> GetInventory(string id, QuestBoardDb db)
    => TypedResults.Ok(await db.Items.AsNoTracking()
        .Where(a => a.CharacterId == id)
        .Select(a => new InventoryItemView
            {
                Id = a.Id,
                Name = a.Template.Name,
                Count = a.Count,
                Description = a.Template.Description
            })
        .ToListAsync());

static async Task<IResult> GetQuests(string id, QuestBoardDb db)
    => TypedResults.Ok(await db.Quests.AsNoTracking().Where(a => a.CharacterId == id)
        .Select(a => new QuestView
            {
                Id = a.Id,
                Name = a.Template.Name,
                ShortDescription = a.Template.ShortDescription,
                CompleteTill = a.CompleteTill,
                Status = a.Status,
                Grade = a.Grade
            })
        .ToListAsync());

static async Task<IResult> GetPremium(string id, QuestBoardDb db)
{
    var character = await db.Characters
        .Include(a => a.Items)
        .ThenInclude(a => a.Template)
        .FirstOrDefaultAsync(a => a.Id == id);

    if (character is null)
        return TypedResults.BadRequest("No character has found!");

    var gold = character.Items.FirstOrDefault(a => a.Template.Id == 57);

    if (gold is null || gold.Count < 400)
        return TypedResults.BadRequest("Не хватает монет!");

    character.PremiumTill = character.PremiumTill.AddDays(7);
    await db.SaveChangesAsync();
    return TypedResults.Ok();
}

static async Task<IResult> GetHistory(string id, QuestBoardDb db)
    => TypedResults.Ok(await db.HistoryRows.Where(a => a.CharacterId == id)
        .OrderByDescending(a => a.Created)
        .Take(15).Select(a => new HistoryRowView
        {
            Id = a.Id,
            Text = a.Text,
            Created = a.Created
        }).ToListAsync());
#endregion

#region Quests

static async Task<IResult> GetQuest(string id, QuestBoardDb db)
    => TypedResults.Ok(await db.Quests.AsNoTracking()
        .Select(a => new QuestDetailsView
        {
            Id = a.Id,
            Name = a.Template.Name,
            Description = a.Template.Description,
            CompleteTill = a.CompleteTill,
            Status = a.Status,
            Grade = a.Grade,
            Rewards = a.Rewards.Select(b => new RewardView
            {
                Id = b.Id,
                Count = b.Count,
                Description = b.Template.ItemTemplate.Description,
                Name = b.Template.ItemTemplate.Name
            }).ToList()
        })
        .FirstOrDefaultAsync(a => a.Id == id));

static async Task<IResult> CompleteQuest(string id, CompleteQuestRequest request, QuestBoardDb db)
{
    var quest = await db.Quests
        .Include(a => a.Rewards)
        .ThenInclude(a => a.Template.ItemTemplate)
        .FirstOrDefaultAsync(a => a.Id == id);

    if (quest is null)
        return TypedResults.BadRequest("Quest not found!");
    
    quest.Status = QuestStatus.Complete;
    var reward = quest.Rewards.FirstOrDefault();

    db.HistoryRows.Add(new HistoryRow
    {
        CharacterId = quest.CharacterId,
        Text = $"Задание выполнено! Получено: {reward}"
    });

    await db.SaveChangesAsync();
    return TypedResults.Ok();
}

static async Task<IResult> RefreshQuest(string id, QuestBoardDb db)
{
    var quest = await db.Quests
        .Include(q => q.Character)
        .FirstOrDefaultAsync(a => a.Id == id);

    if (quest is null)
        return TypedResults.BadRequest("Quest not found!");
    if (quest.Status == QuestStatus.Complete)
        return TypedResults.BadRequest("Выполненный квест нельзя сбрасывать!");
    var date = quest.Character.LastQuestRefresh.AddDays(1);
    if (date > DateTime.Now)
        return TypedResults.BadRequest($"Сбросить квест можно будет не раньше {date}!");
    
    quest.Character.LastQuestRefresh = DateTime.Now;
    await db.SaveChangesAsync();

    var generator = new QuestGenerator();
    await generator.GenerateNew(quest.Character, db);
    db.Quests.Remove(quest);
    await db.SaveChangesAsync();

    return TypedResults.Ok();
}
#endregion

#region Admin

static async Task<IResult> GenerateQuests(GenerateQuestsRequest request, QuestBoardDb db)
{
    var character = await db.Characters.Include(a => a.Quests)
        .FirstOrDefaultAsync(a => a.Id == request.CharacterId);

    if (character is null)
        return TypedResults.NotFound("Пользователь с указанным Id не найден!");

    var generator = new QuestGenerator();
    for (var i = character.Quests.Count; i < 5; i++)
        await generator.GenerateNew(character, db);

    return TypedResults.Ok();
}

static async Task<IResult> DeleteCharacter(string id, QuestBoardDb db)
{
    var character = await db.Characters.FirstOrDefaultAsync(a => a.Id == id);
    
    if (character is not null)
    {
        db.Characters.Remove(character);
        await db.SaveChangesAsync();
    }

    return TypedResults.Ok();
}


static async Task<IResult> DeleteQuest(string id, QuestBoardDb db)
{
    var quest = await db.Quests.FirstOrDefaultAsync(a => a.Id == id);

    if (quest is not null)
    {
        db.Quests.Remove(quest);
        await db.SaveChangesAsync();
    }
    
    return TypedResults.Ok();
}


static async Task<IResult> DeleteQuests(string id, QuestBoardDb db)
{
    var quests = await db.Quests.Where(a => a.CharacterId == id).ToListAsync();
    
    db.Quests.RemoveRange(quests);
    await db.SaveChangesAsync();
    
    return TypedResults.Ok();
}

#endregion

// Configure the HTTP request pipeline.

app.UseSwagger();
app.UseSwaggerUI();
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();